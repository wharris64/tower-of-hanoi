
let selectedDisk; //top disk of currently selected rod elements 
let curRod; // currently selected rod element
let clicked = false; //alternate clicked state, update on click
let winners = []

document.getElementById('reset').addEventListener('click', reset)

let moveCount = 0;
const moveDiv = document.getElementById('moves')

const diskCount = 3

let gameOver = false

// check the widths of the elements a and b
function checkWidths (a,b) {
    let styleA = window.getComputedStyle(a)
    let styleB = window.getComputedStyle(b)
    let widthA = styleA.getPropertyValue('width')
    let widthB = styleB.getPropertyValue('width')
    widthA = parseInt(widthA)
    widthB = parseInt(widthB)
    
    if (widthA < widthB) {
        return true
    } else {
        return false
    }
}

// click handler used to update dom
function updateDisk(event) {
    if(gameOver===false){
        curRod = event.currentTarget
    
        if(!clicked) {
            selectDisk()
        } else {
            moveDisk()
        }
    
        clicked = !clicked
    
        moveCount++
        moveDiv.innerHTML = "Moves: " + moveCount
    
        checkWin()
    }
}

// check for win condition and display win
function checkWin() {
    if(document.getElementById("rod3").childElementCount == diskCount) {
        const winText = document.createTextNode('You won in ' + moveCount + ' moves.')
        const winDiv = document.createElement('div')
        const br = document.createElement('br')
        const imgTag = document.createElement('img')
        imgTag.src = './img/brulewin.gif'
        winDiv.appendChild(winText)
        winDiv.id = 'win'
        document.querySelector('body').appendChild(winDiv)
        winDiv.appendChild(br)
        winDiv.appendChild(imgTag)
        gameOver = true
        // let winPers = prompt("enter name", "this is a name");
        // let winObject = {[winPers]:moveCount}
       
        // winners.push(winObject)
        // console.log(winners)
    }
}

// select top disk from stack
function selectDisk() {
    if (curRod.lastElementChild) {
        selectedDisk = curRod.lastElementChild
        selectedDisk.style.border = "3px solid"
    } else {
        clicked = !clicked
    }
}

// drop selected disk on a stack
function moveDisk() {
    if (selectedDisk) {
        if (curRod.lastElementChild) {
            if (checkWidths(selectedDisk, curRod.lastElementChild)) {
                curRod.appendChild(selectedDisk)
                selectedDisk.style.border = "none"
            } else {
                selectedDisk.style.border = "none"
                selectedDisk = undefined
            }
        } else {
            curRod.appendChild(selectedDisk)
            selectedDisk.style.border = "none"
        }
    }
}

// create disk elements
function initDisks() {
    const rodDiv = document.getElementById('rod1')
    for (let i = 1; i <= diskCount; i++) {
        const diskEl = document.createElement('div')
        const diskText = document.createTextNode('width: '+((diskCount+2)-i)*50)
        diskEl.appendChild(diskText)
        diskEl.classList.add('disk')
        diskEl.id = 'disk'+i
        diskEl.style.width = ((diskCount+2)-i)*50+'px'
        diskEl.style.backgroundColor = getRandomColor()
        rodDiv.appendChild(diskEl)
    }
}

// create rod/stack elements
function initRods() {
    const mainDiv = document.querySelector('main')
    for (let i = 1; i <= 3; i++) {
        const rodEl = document.createElement('div')
        rodEl.classList.add('rod')
        rodEl.id = 'rod'+i
        rodEl.addEventListener('click', updateDisk)
        mainDiv.appendChild(rodEl)
    }
}

// generate random color
function getRandomColor() {
    var letters = '0123456789ABCDEF';
    var color = '#';
    for (var i = 0; i < 6; i++) {
      color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
}

function reset () {
    const rods = Array.from(document.getElementsByClassName('rod'))
    for (let i = 0; i < rods.length; i++) {
        while(rods[i].firstChild) {
            rods[i].removeChild(rods[i].firstChild)
        }
    }
    moveCount = 0
    if(document.getElementById('win')) document.getElementById('win').innerHTML = ''
    gameOver = false
    initDisks()
}

// call init functions to create page
initRods()
initDisks()
